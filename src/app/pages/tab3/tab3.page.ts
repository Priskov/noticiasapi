import { Component } from '@angular/core';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  news: any = []
  numPage: number = 2
  tab:string= 'tab3'


  constructor(private dataLocalService:DataLocalService) {}

  async ngOnInit(){
    await this.dataLocalService.cargarFavoritoss()
    this.news=this.dataLocalService.favs
  }

  ionViewWillEnter() {
    this.tab='tab3'
    this.ngOnInit()
  }
  ionViewWillLeave(){
    this.tab=''
  }
  loadData(event){
    event.target.complete()
  }

}
